import {WebGLRenderer, AnimationMixer, Clock, DirectionalLight, Raycaster, Vector2} from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader";
import { BokehPass } from "three/examples/jsm/postprocessing/BokehPass";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass";


import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";

import { Example } from './example';

interface MyUniform {
    value: any;
}

interface MyUniforms {
    focus: MyUniform;
    aperture: MyUniform;
    maxblur: MyUniform;
}

export default class GLTFExample extends Example {
  mixer?: AnimationMixer;
  composer?: EffectComposer;
  clock: Clock;
  bokehPass?: BokehPass;
  multDelta = {speed: 1.0};

  propsBokeh = {
      focus: 10.0,
      aperture: 0.003,
      maxBlur: 0.004
  };

  raycaster: Raycaster;
  mouse: Vector2;


  constructor(renderer: WebGLRenderer) {
      super(renderer);

      this.clock = new Clock();
      this.raycaster = new Raycaster();
      this.mouse = new Vector2(0,0);
  }

  public initialize() {
    super.initialize();

    this._cam.translateZ(10);

    const directionalLight_1 = new DirectionalLight();
    directionalLight_1.add(directionalLight_1.target);
    directionalLight_1.position.set(0, 0, 0);
    directionalLight_1.target.position.set(8, -8, -12);

    const directionalLight_2 = new DirectionalLight();
    directionalLight_2.add(directionalLight_2.target);
    directionalLight_2.position.set(0, 0, 0);
    directionalLight_2.target.position.set(-8, -10, 12);
    this._scene.add(directionalLight_1, directionalLight_2);

    const renderPass = new RenderPass( this._scene, this._cam );
    this.bokehPass = new BokehPass( this._scene, this._cam, {
         focus: this.propsBokeh.focus,
         aperture: this.propsBokeh.aperture,
         maxblur: this.propsBokeh.maxBlur,
    });

    this.composer = new EffectComposer(this._renderer);
    this.composer.addPass(renderPass)
    this.composer.addPass(this.bokehPass);
    this.composer.setSize(window.innerWidth, window.innerHeight);

    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath( 'lib/draco/' );

    const loaderGLTF = new GLTFLoader();
    loaderGLTF.setDRACOLoader(dracoLoader);
    loaderGLTF.load(
    'assets/models/LittlestTokyo.glb',
        (gltf) => {
          const model = gltf.scene;
          model.position.set(0,0,0)
          model.scale.set( 0.01, 0.01, 0.01 )
          this._scene.add( model )

          this.mixer = new AnimationMixer( model )
          this.mixer.clipAction(gltf.animations[0]).play()
        },
        undefined,
        (err) => console.error(err)
    );

    const animationFolder = this._gui.addFolder("Animation");
    animationFolder.add(this.multDelta, 'speed',  0, 10, 1);

    const dofFolder = this._gui.addFolder("Depth of field");
    dofFolder.add(this.propsBokeh, 'focus',  0, 1000, 1);
    dofFolder.add(this.propsBokeh, 'aperture', 0, 0.005, 0.001);
    dofFolder.add(this.propsBokeh, 'maxBlur',  0, 0.01, 0.001);

    const controls = new OrbitControls(this._cam, this._renderer.domElement);
    controls.update();

    this._renderer.domElement.addEventListener( 'mousemove', (event) => {
        this.mouse.x = ( event.clientX / window.innerWidth ) * 2.0 - 1.0;
        this.mouse.y = ( event.clientY / window.innerHeight ) * 2.0 - 1.0;

        this.raycaster.setFromCamera(this.mouse, this._cam);
        const intersection = this.raycaster.intersectObject(this._scene, true);
        if (intersection && intersection.length > 0) {
            this.propsBokeh.focus = intersection[0].distance
        }
    });
  }

  public destroy(): void {
    super.destroy();
  }

  public resize(w: number, h: number) : void {
      super.resize(w, h);
      this.bokehPass?.renderTargetDepth.setSize(w, h);
      this.composer?.setSize(w, h);
  }

  public render() {
      this.composer?.render();
  }

  public update(): void {
      this.mixer?.update(this.clock.getDelta() * this.multDelta.speed)

      const uniforms = this.bokehPass?.uniforms as MyUniforms;
      uniforms["focus"].value = this.propsBokeh.focus;
      uniforms["aperture"].value = this.propsBokeh.aperture;
      uniforms["maxblur"].value = this.propsBokeh.maxBlur;
  }

}
