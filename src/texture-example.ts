import {
  Mesh,
  TextureLoader,
  DirectionalLight,
  PMREMGenerator,
  RepeatWrapping,
  UnsignedByteType,
  WebGLRenderer,
  MeshPhysicalMaterial,
  PlaneBufferGeometry,
  ShadowMaterial,
} from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import { HDRCubeTextureLoader } from "three/examples/jsm/loaders/HDRCubeTextureLoader";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

import { Example } from './example';

export default class TextureExample extends Example {

  constructor(renderer: WebGLRenderer) {
    super(renderer);
  }

  public initialize() {
    super.initialize();

    this._renderer.shadowMap.enabled = true;

    const directionalLight = new DirectionalLight();
    directionalLight.add(directionalLight.target);
    directionalLight.position.set(0, 0, 0);
    directionalLight.target.position.set(10, -10, -12);
    directionalLight.shadow.mapSize.set(1024,1024);
    directionalLight.shadow.camera.near = -5;
    directionalLight.shadow.camera.far = 8;

    directionalLight.castShadow = true;

    const loaderTexture = new TextureLoader();
    const textureAlbedo = loaderTexture.load('assets/textures/rust/albedo.png');
    textureAlbedo.wrapS = RepeatWrapping;
    textureAlbedo.wrapT = RepeatWrapping;
    const textureNormal = loaderTexture.load('assets/textures/rust/normal.png');
    textureNormal.wrapS = RepeatWrapping;
    textureNormal.wrapT = RepeatWrapping;
    const textureMetallic = loaderTexture.load('assets/textures/rust/metallic.png');
    textureMetallic.wrapS = RepeatWrapping;
    textureMetallic.wrapT = RepeatWrapping;
    const textureRoughness = loaderTexture.load('assets/textures/rust/roughness.png');
    textureRoughness.wrapS = RepeatWrapping;
    textureRoughness.wrapT = RepeatWrapping;

    const planeMaterial = new PlaneBufferGeometry(40,40);
    const shadowPlane = new Mesh(planeMaterial, new ShadowMaterial());
    shadowPlane.position.set(0,-0.55,0);
    shadowPlane.rotation.x =  - Math.PI / 2;
    shadowPlane.receiveShadow = true;
    this._scene.add(shadowPlane)

    const loaderObj = new OBJLoader();
    /*const material = new MeshPhysicalMaterial({
      map: textureAlbedo,
      normalMap: textureNormal,
      metalnessMap: textureMetallic,
      roughnessMap: textureRoughness,
      metalness: 0.5,
      roughness: 0.5
    });
    loaderObj.load(
        'assets/models/material_sphere.obj',
        (object) => {
          object.traverse((child) => {
            if (child instanceof Mesh) {

              child.material = material;
              child.castShadow = true;
            }
          })
          object.position.set(0, 0, 0)
          this._scene.add(object)
        },
        undefined,
        (err) => console.error(err)
    )*/

    for (let x = 0; x < 5; x++) {
      for (let y = 0; y < 5; y++) {

        const material = new MeshPhysicalMaterial({
          map: textureAlbedo,
          normalMap: textureNormal,
          metalnessMap: textureMetallic,
          roughnessMap: textureRoughness,
          metalness: y * 0.2,
          roughness: x * 0.2
        });

        loaderObj.load(
            'assets/models/material_sphere.obj',
            (object) => {
              object.traverse((child) => {
                if (child instanceof Mesh) {

                  child.material = material;
                  child.castShadow = true;
                }
              })
              object.position.set(x * 1.5 - 3, y * 1.5, 0)
              this._scene.add(object)
            },
            undefined,
            (err) => console.error(err)
        )
      }
    }

    const pmremGenerator = new PMREMGenerator(this._renderer);
    pmremGenerator.compileCubemapShader();
    const hdrTexture = new HDRCubeTextureLoader()
        .setPath('assets/env/pisa/')
        .setDataType(UnsignedByteType)
        .load(['px.hdr', 'nx.hdr', 'py.hdr', 'ny.hdr', 'pz.hdr', 'nz.hdr'], () => {
          const target = pmremGenerator.fromCubemap(hdrTexture);
          this._scene.environment = target.texture;
          this._scene.background = target.texture;
        });

    this._scene.add(directionalLight);

    /*const cameraHelper = new CameraHelper(directionalLight.shadow.camera);
    const helper = new DirectionalLightHelper(directionalLight);
    this._scene.add(helper, cameraHelper);*/

    const controls = new OrbitControls(this._cam, this._renderer.domElement);
    controls.update();
  }

  public destroy(): void {
    super.destroy();
  }

  public update(): void {
  }

}
