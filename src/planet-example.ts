import { Mesh, MeshBasicMaterial, MeshLambertMaterial, OctahedronBufferGeometry, PointLight, SphereBufferGeometry, TorusBufferGeometry, WebGLRenderer } from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

import { Example } from './example';

export default class PlanetsExample extends Example {

  earthMesh?: Mesh
  marsMesh?: Mesh

  sunGeom?: SphereBufferGeometry
  sunMaterial?: MeshBasicMaterial

  earthGeom?: OctahedronBufferGeometry
  earthMaterial?: MeshLambertMaterial
  earthOrbitGeom?: TorusBufferGeometry

  orbitMaterial?: MeshBasicMaterial

  marsGeom?: OctahedronBufferGeometry
  marsMaterial?: MeshLambertMaterial
  marsOrbitGeom?: TorusBufferGeometry

  controls: OrbitControls

  speedSystemRotation = {speedSystemRotation: 0.002}
  speedEarthRotation = {speedEarthRotation: 0.005}
  speedMarsRotation = {speedMarsRotation: 0.01}
  intensitySunLight = {intensitySunLight: 1}

  constructor(renderer: WebGLRenderer) {
    super(renderer);

    this.controls = new OrbitControls(this._cam, this._renderer.domElement) as OrbitControls;
  }

  public initialize(): void {
    super.initialize();

    this.sunGeom = new SphereBufferGeometry(0.2, 32, 16) as SphereBufferGeometry;
    this.earthGeom = new OctahedronBufferGeometry(0.1, 2) as OctahedronBufferGeometry;
    this.earthOrbitGeom = new TorusBufferGeometry(0.5, 0.005, 30, 90) as TorusBufferGeometry;
    this.marsGeom = new OctahedronBufferGeometry(0.05, 5) as OctahedronBufferGeometry;
    this.marsOrbitGeom = new TorusBufferGeometry(1, 0.005, 30, 90) as TorusBufferGeometry;

    this.sunMaterial = new MeshBasicMaterial() as MeshBasicMaterial;
    this.earthMaterial = new MeshLambertMaterial({ color: 0x2AD795 }) as MeshLambertMaterial;
    this.marsMaterial = new MeshLambertMaterial({ color: 0xEB5526 }) as MeshLambertMaterial;

    this.orbitMaterial = new MeshBasicMaterial({ color: 0xB4B4B4 }) as MeshBasicMaterial;


    const sunLight = new PointLight(0xffffff) as PointLight
    sunLight.position.set(0, 0, 0)
    sunLight.decay = 2
    sunLight.distance = 15
    sunLight.intensity = this.intensitySunLight.intensitySunLight

    const sunMesh = new Mesh(this.sunGeom, this.sunMaterial)
    sunMesh.material.color.setRGB(1, 1, 0)

    this.earthMesh = new Mesh(this.earthGeom, this.earthMaterial)
    this.earthMesh.position.set(0.5, 0, 0)

    this.marsMesh = new Mesh(this.marsGeom, this.marsMaterial)
    this.marsMesh.position.set(-1, 0, 0)

    const earthOrbitMesh = new Mesh(this.earthOrbitGeom, this.orbitMaterial)
    const marsOrbitMesh = new Mesh(this.marsOrbitGeom, this.orbitMaterial)

    this._scene.add(sunMesh)
    this._scene.add(sunLight)

    this._scene.add(this.earthMesh)
    this._scene.add(earthOrbitMesh)
    
    this._scene.add(this.marsMesh)
    this._scene.add(marsOrbitMesh)

    this.controls.update();

    const systemFolder = this._gui.addFolder('Solar systeme')
    systemFolder.add(this.speedSystemRotation, 'speedSystemRotation', 0, 0.5)
    systemFolder.add(this.speedEarthRotation, 'speedEarthRotation', 0, 0.5)
    systemFolder.add(this.speedMarsRotation, 'speedMarsRotation', 0, 0.5)
    systemFolder.add(this.intensitySunLight, 'intensitySunLight', 0, 10, 0.01)
  }

  public destroy(): void {
    super.destroy();
  }

  public update(delta: number, _: number): void {
    this.earthMesh!.rotation.y += this.speedEarthRotation.speedEarthRotation;
    this.marsMesh!.rotation.y += this.speedMarsRotation.speedMarsRotation;
    this._scene.rotation.z -= this.speedSystemRotation.speedSystemRotation;

    this.controls.update();
  }

}
